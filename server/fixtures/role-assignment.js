var requesters = require('./requesters');
var roleAndAttributes = require('./roleAndAttributes');

var collection = Object.keys(requesters).reduce(function(acc, ou) {
    // Iterate Per OU
    acc[ou] = requesters[ou].map(roleAndAttributes);
    return acc;
}, {});

function filterByParams(ou, role, skip, limit) {

    var result;

    // Admit 'all' parameters
    if (ou == 'all' && role == 'all') {
        result = collection;
    } else {

        // Filter the collection

        function byRoleFilter(item) {
            return item.businessRole.businessRole == role
        }

        // FilterByOU
        var  filteredByOU = {};
        if (ou != "all") {
            // Just the selected OU
            filteredByOU[ou] = collection[ou];
        } else {
            filteredByOU = collection;
        }

        // FilterByRole
        if (role != "all") {
            // For each ou, get the role filtered collection
            filteredByOU = Object.keys(filteredByOU).reduce(function(acc, ou) {
                acc[ou] = filteredByOU[ou].filter(byRoleFilter);
                return acc;
            }, {});
        }

        result = filteredByOU;
    }

    // Skip and Limit the result
    function skipAndLimit(result) {

        if (isNaN(skip) || isNaN(limit)) {

            return result;

        }

        // Traverse OU
        result = Object.keys(result).reduce(function(acc, ou){
            var objs = result[ou].map(function(i) {return {ou: ou, obj: i}});
            return acc.concat(objs);
        }, []);

        var totalAmount = result.length;

        var startIndex = skip;
        var endIndex = skip + limit;

        console.log(startIndex);
        console.log(endIndex);


        if (startIndex > totalAmount) {
            return [];
        }

        // Fix indexes
        if (endIndex > (totalAmount - startIndex)) {
            endIndex = totalAmount - startIndex;
        }

        if (endIndex - startIndex < 0) {
            return [];
        }

        result = result.slice(startIndex, endIndex);

        // Untraverse OU
        result = result.reduce(function(acc, x){
            var ou = x.ou;
            var item = x.obj
            if (!acc[ou]) acc[ou] = [];
            acc[ou] = acc[ou].concat(item);
            return acc;
        }, {});

        return result;

    }

    return skipAndLimit(result);
}

module.exports = filterByParams
