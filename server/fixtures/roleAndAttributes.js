var businessRoles = require('./businessRoles');
var attributeList = require('./attributeList');

module.exports = function (userId) {

    var businessRole = businessRoles[Math.floor(Math.random()*businessRoles.length)];

    return {
        "createdBy": "ALVARM1H",
        "crTime": "2014-12-05T00:00:00+01:00",
        "lmTime": "2014-12-05T00:00:00+01:00",
        "updatedBy": "ALVARM1H",
        "businessRole": businessRole,
        "id": 1010,
        "attributeList": attributeList,
        "userId": userId
    }
}
