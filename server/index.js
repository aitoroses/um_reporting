var Nocker = require('nocker');

var routes = [

    // BusinessRoles
    {
        method: 'GET',
        path: '/umms/reporting/business_role_assignment/:ou/:role', // query: ?skip&limit. Note: Role and OU can be 'all'
        reply: function(params, query, body) {

            if (query.describe) {
                return {
                    ou: params.ou,
                    role: params.role,
                    query: query
                }
            } else {

                // Get the entire collection by OU
                var getResults = require('./fixtures/role-assignment');

                var limit = parseInt(query.limit);
                var skip = parseInt(query.skip);
                var ou = params.ou;
                var role = params.role;

                var results = getResults(
                    ou,
                    role,
                    skip,
                    limit
                )

                return results;
            }
        }
    },

    // BusinessRoles
    {
        method: 'GET',
        path: '/UMServices/userproperties/getlang',
        reply: function(params, query, body) {
            return require('./fixtures/getLang');
        }
    },
];

// Register routes to Nocker
Nocker.register(routes);

// Default ROUTE showing all routes
Nocker.register([{
    method: 'GET',
    path: '/',
    reply: function() {
        return routes
    }
}])

// Start server on port 7003
Nocker.start({delay: 500, port: 7003, auth: false}, function() {
  console.log("Server is listening on port " + this.port + '\n');
})
