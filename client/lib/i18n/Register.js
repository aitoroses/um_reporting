import {parse} from './parser';
import * as i18n from './';

export function Register(lang, properties) {
  // Parse properties
  var labels = parse(properties);

  var dict = i18n.getDict(lang);
  if (!dict) {
    dict = i18n.addLang(lang);
  }

  // Write the labels
  Object.keys(labels).forEach((k) => { dict[k] = labels[k] });
}
