
export function parse(data) {
  let result = data
  .split(/\n/g)
  .filter(function(l) {
    if (l[0] == '#' || encodeURIComponent(l) == "%0D") {
      return false;
    } else {
      return true;
    }
  })
  .reduce(function(final, w) {
    if (!w) return final;
    let i = w.indexOf('=');
    let a = [w.slice(0,i), w.slice(i+1)];
    final[a[0].trim()] = a[1].trim();
    return final;
  }, {});
  return result;
}
