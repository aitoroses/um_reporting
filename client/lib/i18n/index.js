var dict = {};

var lang = {
  userLang: ""
}

export function translate(key) {

  var d = getDict();
  var translated = d ? d[key]: undefined;
  if (d && translated) {
    return translated;
  } else {
    if (process.env.NODE_ENV != "production") {
      return "Unknown i18n: " + key;
    } else {
      return key;
    }
  }
}

export var t = translate;

export function getDict() {
  return dict[lang.userLang]
};

export function getLang() {
  return lang.userLang;
}

export function addLang(lang) {
  if (!dict[lang]) {
    dict[lang] = {};
  }
  return dict[lang];
}

export function setLang(lng) {
  lang.userLang = lng;
}

export function getAllDicts() {
  return dict;
}

export function setAllDicts(dicts) {
  dict = dicts;
}

export {Register} from './Register';

export {getBrowserLang} from './getBrowserLang';
