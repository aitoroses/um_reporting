export function getBrowserLang() {
  var nav = window.navigator,
  browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'],
  i,
  language;

  // support for HTML 5.1 "navigator.languages"
  if ($.isArray(nav.languages)) {
   for (i = 0; i < nav.languages.length; i++) {
     language = nav.languages[i];
     if (language && language.length) {
       return language.substr(0,2).toLowerCase();
     }
   }
  }

  // support for other well known properties in browsers
  for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
   language = nav[browserLanguagePropertyKeys[i]];
   if (language && language.length) {
     return language.substr(0,2).toLowerCase();
   }
  }
}
