var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var webpackConfig = require('./webpack.config');

// Build the bundle
gulp.task('build', function(done) {

  var entry = './src/index.js';
  var output = {
    library: 'ReportingViewComponent',
    libraryTarget: 'umd',
    filename: 'umm-reporting.js',
    path: './build'
  }
  webpackConfig.entry = entry;
  webpackConfig.output = output;

  // run webpack
  webpack(webpackConfig, function(err, stats) {
      if(err) throw new gutil.PluginError("webpack", err);
      gutil.log("[webpack]", stats.toString({
          // output options
      }));
      done();
  });

});

gulp.task("dev", function(callback) {

  webpackConfig.entry = [
    './src/index.js'
  ]

  // Development dependencies
  if (!process.env.COMPRESS) {
    webpackConfig.entry.unshift('webpack-dev-server/client?http://localhost:8080')
  }

  webpackConfig.output = {
    path: __dirname,
    library: 'ReportingViewComponent',
    libraryTarget: 'umd',
    publicPath: "/",
    filename: "build/bundle.js"
  }

  // Start a webpack-dev-server
  var compiler = webpack(webpackConfig);

  new WebpackDevServer(compiler, {

        // server and middleware options
        // hot: true

  }).listen(8080, "localhost", function(err) {
      if(err) throw new gutil.PluginError("webpack-dev-server", err);
      // Server listening
      gutil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server/index.html");

      // keep the server alive or continue?
      // callback();
  });
});

gulp.task('default', ['dev'])
