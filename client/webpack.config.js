var webpack = require('webpack');
var path = require('path');
var node_modules = path.resolve('node_modules');
var pathToReact = path.resolve(node_modules, 'react/dist/react-with-addons.js');

var plugins = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
  })
];

if (process.env.COMPRESS) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  );
}

module.exports = {

  externals: [
    {
      "jquery": "jQuery"
    }
  ],

  module: {
    loaders: [
      { test: /\.js$/, loaders: ['babel?stage=0'], exclude: /node_modules/ },
      { test: /\.css$/, loaders: ['style', 'css'] },
    ],
    noParse: [pathToReact]
  },

  resolve: {
    root: [node_modules],
    extensions: ['', '.js', '.jsx',],
    alias: {
      "react/lib": path.resolve(node_modules, 'react/lib'),
      "react": pathToReact
    }
  },

  node: {
    Buffer: false
  },

  plugins: plugins,

  devtool: process.env.COMPRESS ? null : 'inline-source-map'

};
