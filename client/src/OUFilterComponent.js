import React from 'react';
import './i18n';
import {t} from '../lib/i18n';

export class OUFilterComponent extends React.Component {

    static propTypes = {
        data: React.PropTypes.array.isRequired
    }

    handleChangeOUFilter(sender){
        var filter = sender.target.value;

        actions.setFiltro(filter);
    }

    renderOuOptions() {
      var datos = this.props.data;
      var optionsOUs = (datos.length != 0) ? store.getOus().map((option, i)=>{
        return <option key={i} value={option}>{option}</option>
      }) : [];

      optionsOUs.unshift(<option key="default" value="-1">-- Select an OU --</option>)
      return optionsOUs;
    }

    render(){
        return (
            <label>{t("table.filters.label.ou")}
                <select className="form-control" onChange={ this.handleChangeOUFilter.bind(this) }>
                    {this.renderOuOptions()}
                </select>
            </label>
        );
    }
}
