import React from 'react';
import $ from 'jquery';
import {Table, Column} from 'fixed-data-table';
import {AttributeColumnCell} from './AttributeColumnCell';
import {OUFilterComponent} from './OUFilterComponent';
import {RoleFilterComponent} from './RoleFilterComponent';
import {InputFilterComponent} from './InputFilterComponent';
import {state, store, actions} from './AppState';
import './i18n';
import {t} from '../lib/i18n';

class ReportingTable extends React.Component {

    static propTypes = {
        data: React.PropTypes.array.isRequired,
        getWidth: React.PropTypes.func.isRequired
    }

    sortByRole() {
        if(store.getSort()){
            actions.setSort(false);
        }else{
            actions.setSort(true);
        }
        actions.setPagActual(1);
    }

    handleChangeRoleFilter(sender){
        var options = [].slice.call(sender.currentTarget.selectedOptions);
        var optionsSelected = options.map((e)=>{
            return e.value;
        });

        actions.setFiltroRole(optionsSelected);
        actions.setPagActual(1);
    }

    renderAttributes(attributes) {
        return React.addons.createFragment({
            table: <AttributeColumnCell attributes={attributes} />
        });
    }

    renderRoleHeader() {
        return <a href="javascript:void(0)" onClick={this.sortByRole.bind(this)}>{t("table.column.role")} <span>&darr;</span></a>;
    }

    handleResize = () => {
      actions.setWidthTotal(this.props.getWidth());
    }

    componentWillUnmount() {
        $(window).off("resize", this.handleResize);
    }

    componentDidMount() {
      actions.setWidthTotal(this.props.getWidth());
      $(window).resize(this.handleResize);
    }

    recalculateWidthAttr() {
        var maxW = 0;
        $(".attributesParent").each(function(){
            var total = 0;
            $("span:first-child", this).each(function(){
                total += $(this).width();
            });
            if(total > maxW){
                maxW = total;
            }
        });

        actions.setWidthAttr(maxW+100);
    }

    _onFilterChange(e) {
        var f = e.target.value;

        actions.setFiltroRole(f);
    }

    _onColumnResizeEndCallback(newColumnWidth, dataKey) {
        actions.setWidthAttr(newColumnWidth);
        actions.setIsColumnResizing(false);
    }

    getClassPrevBtn() {
        var classes = "btn btn-default";
        if(store.getPagActual() == 1){
            classes += " disabled";
        }
        return classes;
    }

    getClassNextBtn(lengthFiltered) {
        var classes = "btn btn-default";

        if((store.getPagActual()*store.getNItems()) > lengthFiltered){
            classes += " disabled";
        }
        return classes;
    }

    nextPage(){
        actions.setPagActual(store.getPagActual()+1);
    }

    prevPage(){
        if(store.getPagActual() > 1){
            actions.setPagActual(store.getPagActual()-1);
        }
    }

    getItemSiguiente() {
        if(store.getPagActual() == 1){
            return 0;
        }else{
            return store.getPagActual()*store.getNItems()-store.getNItems();
        }
    }



    renderRoleOptions() {
      var datos = this.props.data;

      var optionsRoles = datos
           .reduce((acc,userRole) => {
              var role = userRole.data.businessRole.businessRole;
              var exists = acc.indexOf(role) > -1;
              if (!exists) {
                  acc.push(role);
              }
              return acc;
           }, [])
           .map( role => <option key={role} value={role}>{role}</option> );

      return optionsRoles;
    }

    render() {
        var datos = this.props.data;

        //Filter by OU
        var filtered = datos.filter((i) => store.getFiltro() ? i.ou == store.getFiltro() : true);


        //Sort
        if(store.getSort()){
            filtered = filtered.sort(function(a, b) {
                return a.data.businessRole.businessRole.localeCompare(b.data.businessRole.businessRole);
            });
        } else {
            filtered = filtered.sort(function(a, b) {
                return b.data.businessRole.businessRole.localeCompare(a.data.businessRole.businessRole);
            });
        }

        //Filter by Role
        filtered = store.getFiltroRole().length > 0 ? filtered.filter((row)=>{
            var items = store.getFiltroRole();
            return items.indexOf(row.data.businessRole.businessRole) >= 0;
        }) : filtered;

        if(filtered.length>0){
            this.recalculateWidthAttr();
        }

        var datosG = filtered;
        filtered = datosG.slice(this.getItemSiguiente(), store.getNItems()*store.getPagActual());

        //Full search
          var search = store.getFiltroGeneral();
          if(search){
              var searchRegExp = new RegExp(search, "gi");
              filtered = filtered.filter(e => JSON.stringify(e).match(searchRegExp));
          }

        return (
            <div>

                <p className="filters">
                    <OUFilterComponent data={datos} /><RoleFilterComponent data={datos} />
                </p>

                <div className="pagination_table">
                    <label>{t("table.filters.label.nitems")}
                        <input type="text" className="form-control" onChange={(e)=>{
                                actions.setPagActual(1);
                                if(!e.target.value){
                                }else if(!isNaN(e.target.value)){
                                    actions.setNItems(e.target.value);
                                }else{
                                    actions.setNItems(10);
                                }
                            }} value={store.getNItems()} maxLength="5" />
                    </label>
                    <span className="btns">
                        <button className={this.getClassPrevBtn()} onClick={this.prevPage}>&lt;</button>
                        <span className="btn btn-default disabled">{store.getPagActual()} / {Math.ceil(datosG.length/store.getNItems())}</span>
                        <button className={this.getClassNextBtn(datosG.length)} onClick={this.nextPage}>&gt;</button>
                    </span>
                    <InputFilterComponent data={datos} />
                </div>

                <Table
                rowHeight={50}
                rowGetter={(i) => filtered[i]}
                rowsCount={filtered.length}
                width={store.getWidthTotal()}
                height={490}
                headerHeight={50}
                overflowX={"auto"}
                overflowY={"auto"}>
                    <Column label={t("table.column.organizationalunit")} width={200} fixed={true} dataKey={"ou"} />
                    <Column label={t("table.column.userid")} dataKey="userId" width={200} fixed={true} cellRenderer={(cellData,cellDataKey,rowData,rowIndex) => rowData.data.userId} />
                    {/*<Column label="USER FIRSTNAME" width={200} fixed={true} dataKey={'createdBy'}  />*/}
                    <Column label={t("table.column.role")} headerRenderer={this.renderRoleHeader.bind(this)} width={200} dataKey="businessRole" cellRenderer={(cellData,cellDataKey,rowData,rowIndex) => rowData.data.businessRole.businessRole} />
                    <Column label={t("table.column.attributes")} dataKey="attributes" width={store.getWidthAttr()} cellRenderer={(cellData,cellDataKey,rowData,rowIndex) => this.renderAttributes.call(this, rowData.data.attributeList) } />
                    <Column label={t("table.column.profile")} dataKey="" width={150} cellRenderer={(cellData,cellDataKey,rowData,rowIndex) => <a key={rowIndex} href={"https://search.novartis.net/Pages/PeopleResults.aspx?k=" + rowData.data.userId}>{t("table.column.gotoprofile")}</a>} />
                </Table>

            </div>
        );
    }
}

export {
    ReportingTable
};
