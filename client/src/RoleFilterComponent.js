import React from 'react';
import './i18n';
import {t} from '../lib/i18n';

export class RoleFilterComponent extends React.Component {

    static propTypes = {
        data: React.PropTypes.array.isRequired
    }

    handleChangeRoleFilter(sender){
        var options = [].slice.call(sender.currentTarget.selectedOptions);
        var optionsSelected = options.map((e)=>{
            return e.value;
        });

        actions.setFiltroRole(optionsSelected);
        actions.setPagActual(1);
    }

    renderRoleOptions() {
      var datos = this.props.data;

      var optionsRoles = datos
           .reduce((acc,userRole) => {
              var role = userRole.data.businessRole.businessRole;
              var exists = acc.indexOf(role) > -1;
              if (!exists) {
                  acc.push(role);
              }
              return acc;
           }, [])
           .map( role => <option key={role} value={role}>{role}</option> );

      return optionsRoles;
    }

    render(){
        return (
            <label>{t("table.filters.label.role")}
                <select multiple="true" className="form-control" onChange={ this.handleChangeRoleFilter.bind(this) }>
                    {this.renderRoleOptions()}
                </select>
            </label>
        );
    }
}
