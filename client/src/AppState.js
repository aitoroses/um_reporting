import React from 'react';
import Tessel from 'tessel-js';
import request from 'tessel-js/lib/request';

import csvExport from './csvExport';

// Create a tessel instance
export var state = new Tessel();

// Create a store
class ReportingStore {

    state = {
        data: {},
        reducedData: [],
        filtro: "",
        filtroGeneral: "",
        filtroRole: [],
        sort: false,
        widthTotal: 1000,
        widthAttr: 300,
        isColumnResizing: false,
        pagActual: 1,
        nItems: 10,
        server: ''
    }

    constructor() {
        this.bindState('reporting');
    }

    getData() {
        return this.state.data;
    }

    getReducedData() {
        return this.state.reducedData;
    }

    getOus() {
        return Object.keys(this.state.data);
    }

    getFiltro() {
        return this.state.filtro;
    }

    getFiltroGeneral() {
        return this.state.filtroGeneral;
    }

    getFiltroRole() {
        return this.state.filtroRole;
    }

    getSort() {
        return this.state.sort;
    }

    getWidthTotal() {
        return this.state.widthTotal;
    }

    getWidthAttr() {
        return this.state.widthAttr;
    }

    getIsColumnResizing() {
        return this.state.isColumnResizing;
    }

    getPagActual() {
        return this.state.pagActual;
    }

    getNItems() {
        return this.state.nItems;
    }
}

// Create actions
class ReportingActions {
    setFiltro(nFiltro) {
        this.set({
            filtro: (nFiltro != -1) ? nFiltro : false
        });
    }
    setFiltroGeneral(nFiltroGeneral) {
        this.set({
            filtroGeneral: nFiltroGeneral
        });
    }
    setFiltroRole(nFiltroRole) {
        var filters = nFiltroRole;

        this.set({
            filtroRole: filters
        });
    }
    setSort(nSort) {
        this.set({
            sort: nSort
        });
    }
    setWidthTotal(nWidth) {
        this.set({
            widthTotal: nWidth
        });
    }
    setWidthAttr(nWidth) {
        this.set({
            widthAttr: nWidth
        });
    }
    setIsColumnResizing(nIsColumnResizing) {
        this.set({
            isColumnResizing: nIsColumnResizing
        });
    }
    setPagActual(pActual) {
        this.set({
            pagActual: pActual
        });
    }
    setNItems(nNItems) {
        if(isNaN(nNItems)){
            this.set({
                nItems: 1
            });
        }else{
            this.set({
                nItems: parseInt(nNItems)
            });
        }
    }

    fetchReportingData(_, resolve) {
        request.get(this.server)
        .then(({body}) => {

            var ouReducer = (acc, ou) => {
                return acc.concat(body[ou].map((i) => {
                    return {
                        ou: ou,
                        data: i
                    }
                }));
            }

            function transformToCollection(data) {
                var result = Object.keys(data)
                    .reduce( ouReducer, [])
                return result;
            }

          // Set state ( Modifiying AppState )
          // Get a fresh state, it happens that this action it's first triggered
          // By the componentDidMount of the main componentDidMount
          // TODO: Bug in tessel, "this" should be always a reference to state.get()[namespace]

          // this.set({
          state.get().reporting.set({
            data: body,
            reducedData: transformToCollection(body)
          })

          resolve(body);
      });
    }

    /*getExportData(data) {

        var filtered = data.filter((i) => this.state.filtro ? i.ou == this.state.filtro : true);

        if(this.state.sort){
            filtered = filtered.sort(function(a, b) {
                return a.data.businessRole.businessRole.localeCompare(b.data.businessRole.businessRole);
            });
        } else {
            filtered = filtered.sort(function(a, b) {
                return b.data.businessRole.businessRole.localeCompare(a.data.businessRole.businessRole);
            });
        }



        var cols = ["Organizational Unit", "User ID", "Role", "Attributes"];
        var transformedData = [cols];


        data.forEach(function(d){

            var n = [];
            n.push(d["ou"]);
            n.push(d["data"]["userId"]);
            n.push(d["data"]["businessRole"]["businessRole"]);
            var strAttr = "";
            d["data"]["attributeList"].forEach(function(a){
                strAttr += a["name"] + ":" + a["value"] + " | ";
            });
            strAttr.substring(0, strAttr.length-1);
            n.push(strAttr);

            transformedData.push(n);
        });
        return transformedData;
    }*/

    downloadCSV() {
        return true;
        //csvExport(this.getExportData(filterData));
    }

    setServer(server) {
      this.set({'server': server});
    }

    setAppId(appId) {
      this.set({'appId': appId});
    }
}

// Export instances
export var store = window.store = state.createStore(ReportingStore);
export var actions = window.actions = store.createActions(ReportingActions);
