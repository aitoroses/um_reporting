import React from 'react';

export class AttributeColumnCell extends React.Component {

    static propTypes = {
        attributes: React.PropTypes.array.isRequired
    }

    renderAttributeColumn(label, value, i) {
        return (
          <span key={i}>
            <span className="header">{label}</span>
            <br />
            <span className="body">{value}</span>
          </span>
        )
    }

    componentDidMount() {
        var node = React.findDOMNode(this);
        node.parentNode.style["padding"] = 0;
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {

        var attributes = this.props.attributes;

        return (

            <div className="attributesParent">
                {attributes.map((c, i) => this.renderAttributeColumn(c.name, c.value, i))}
            </div>
        )
    }
}
