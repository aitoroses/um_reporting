import React from 'react';

import {state, store, actions} from './AppState';
import {Button} from './Button';
import {ReportingTable} from './ReportingTable';

// TODO: Load labels from UM (DB)
import {setupI18n} from './i18n';
import {t} from '../lib/i18n';

// Styles loading
require('fixed-data-table/dist/fixed-data-table.css')
require('../css/style.css');

// Expose React
global.React = React;

class ReportingViewComponent extends state.Component {

  static propTypes = {
    i18n: React.PropTypes.object.isRequired,
    datasource: React.PropTypes.string.isRequired,
    appId: React.PropTypes.string.isRequired
  }

  componentDidMount() {
    super.componentDidMount();

    setTimeout(() => {
      // Configure server endpoint
      actions.setServer(this.props.datasource);

      // Configure the AppId
      actions.setAppId(this.props.appId);

      // Fetch the data
      actions.fetchReportingData();
    })
    // Setup internationalization
    setupI18n(
      this.props.i18n.dict,
      this.props.i18n.lang
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.reporting != nextState.reporting
  }

  getParentWidth() {
    return React.findDOMNode(this).parentNode.clientWidth;
  }

  render() {
    if (!this.state.reporting) return <span></span>
    return (
      <div className="reporting">
        <ReportingTable
          data={this.state.reporting.reducedData}
          getWidth={this.getParentWidth.bind(this)} />
      </div>

    )
  }

}

export default ReportingViewComponent;
