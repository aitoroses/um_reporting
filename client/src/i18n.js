var i18n = require('../lib/i18n');

export function setupI18n(dicts, lang) {
  i18n.setAllDicts(dicts);

  // Set users navigator preferred language
  i18n.setLang(lang);
}
