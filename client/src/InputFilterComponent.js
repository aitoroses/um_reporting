import React from 'react';
import './i18n';
import {t} from '../lib/i18n';

export class InputFilterComponent extends React.Component {

    static propTypes = {
        data: React.PropTypes.array.isRequired
    }

    handleChangeFilter(sender){
        actions.setFiltroGeneral(sender.currentTarget.value);
    }

    render(){
        return (
            <label>
                <input type="text" placeholder={t("table.filters.label.fullsearch")} className="form-control search" onChange={this.handleChangeFilter.bind(this)} />
            </label>
        );
    }
}
