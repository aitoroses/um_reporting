import React from 'react';

// Create a button component
export class Button extends React.Component {

  static propTypes = {
    clickHandler: React.PropTypes.func.isRequired,
    label: React.PropTypes.string
  }

  render() {
    return (
      <button
        className="btn btn-primary"
        onClick={this.props.clickHandler}>
        {this.props.label}
      </button>
    )
  }

}
